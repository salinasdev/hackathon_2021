import { html, LitElement } from "lit";

class TiendaData extends LitElement {

    static get properties(){
        return {
            holaMundo : {type: String},
            products : {type: Array},
            delProd : {type: String},
            product: {type: Object},
            updProduct: {type: Object},
            damePedido: {type: Boolean},
            pedidos: {type: Array}
        };
    }

    constructor(){
        super();

        //this.holaMundo = "ee";

        this.delProd = "0";
        this.product = {};
        this.updProduct = {};
        this.damePedido = false;

        //this.pedidos = [{"id": 1,"productos": [{"id": "1","name": "Limones","desc": "Producto 1","price": 10.0,"cant": 10},{"id": "2","name": "Naranjas","desc": "Producto 2","price": 20.0,"cant": 20},{"id": "3","name": "Peras","desc": "Producto 3","price": 30.0,"cant": 2}]}];
        
        this.pedidos = [];

        //this.products = [{"id":"1","name":"Limoneds","desc":"Producto 1","price":10.0,"cant":10},{"id":"2","name":"Kiwis","desc":"Producto 2","price":20.0,"cant":20},{"id":"3","name":"Manzanas","desc":"Producto 3","price":30.0,"cant":0},{"id":"4","name":"Peras","desc":"Producto 1","price":23.0,"cant":40}];
        this.products = [];

        this.getDataAPi();

    }

    getDataAPi(){
        console.log("Estoy en getDataAPi");
        console.log("Obteniendo datos de la API");

        let xhr = new XMLHttpRequest();

        //Propiedad de XHR que se lanza cuando obtiene un resultado de la petición
        xhr.onload = () => {
            if (xhr.status === 200){
                console.log("Petición completada correctamente");
                
                //Parseamos el JSON que nos llega para visualizarlo en la consola
                console.log(JSON.parse(xhr.responseText));

                //Metemos en APIResponde el JSON
                let APIResponse = JSON.parse(xhr.responseText);

                //Asignamos a movies el array que viene en results
                this.products = APIResponse;

                //Esto es el hola mundo y lo movemos tal cual, es una cadena de texto.
                //this.holaMundo = xhr.responseText;
            }
        }

        //Creamos la petición
        xhr.open("GET","http://localhost:8080/hackaton/products");
        //Enviamos la petición
        xhr.send();


        console.log("FIN getDataAPi");
    }

    updated(changedProperties){
        console.log("updated tienda-data");

        if(changedProperties.has("products")){
            console.log("Ha cambiado el valor de la propiedad products");

            this.dispatchEvent(
                new CustomEvent(
                    "products-data-updated",
                    {
                        detail : {
                            products : this.products
                        }
                    }
                )
            );
        }else{
            if(changedProperties.has("delProd")){
                console.log("Se ha cambiado la propiedad delProd");
                this.deleteProduct();
            }
            if(changedProperties.has("product")){
                console.log("Se ha cambiado la propiedad product");
                this.newProduct();
            }
            if(changedProperties.has("updProduct")){
                console.log("Se ha cambiado la propiedad updProduct");
                console.log(this.updProduct);
                this.updateProduct();
            }
            if(changedProperties.has("damePedido")){
                console.log("Se ha cambiado la propiedad damePedido");
                this.damePedido = false;
                this.getOrders();
                
            }
            if(changedProperties.has("pedidos")){
                this.dispatchEvent(
                    new CustomEvent(
                        "orders-data-updated",
                        {
                            detail : {
                                pedidos : this.pedidos
                            }
                        }
                    )
                );
            }

        }

    }

    deleteProduct(){
        console.log("Estoy en deleteProduct");

        let xhr = new XMLHttpRequest();

        //Propiedad de XHR que se lanza cuando obtiene un resultado de la petición
        xhr.onload = () => {
            if (xhr.status === 200){
                console.log("Petición completada correctamente");
                
                //Una vez borrado recargamos el array
                this.getDataAPi();
            }
        }

        //Creamos la petición
        xhr.open("DELETE","http://localhost:8080/hackaton/products/" + this.delProd);
        //Enviamos la petición
        xhr.send();

        console.log("FIN deleteProduct");

    }

    newProduct(){
        console.log("Estoy en newProduct en tienda-data");
        console.log(this.product);

        //Nos aseguramos de no enviar el idp
        delete this.product.idp;

        let xhr = new XMLHttpRequest();

        //Propiedad de XHR que se lanza cuando obtiene un resultado de la petición
        xhr.onload = () => {
            if (xhr.status === 201){
                console.log("Petición completada correctamente newProduct");
                
                //Una vez borrado recargamos el array
                this.getDataAPi();
            }
        }

        //Creamos la petición
        xhr.open("POST","http://localhost:8080/hackaton/products/");

        console.log(JSON.stringify(this.product));

        xhr.setRequestHeader("Content-Type", "application/json");

        //Enviamos la petición con el objeto nuevo
        xhr.send(JSON.stringify(this.product));

        console.log("FIN newProduct");

    }

    updateProduct(){
        console.log("Estoy en updateProduct en tienda-data");
        console.log(this.updProduct);

        let xhr = new XMLHttpRequest();

        //Propiedad de XHR que se lanza cuando obtiene un resultado de la petición
        xhr.onload = () => {
            if (xhr.status === 200){
                console.log("Petición completada correctamente newProduct");
                
                //Una vez borrado recargamos el array
                this.getDataAPi();
            }
        }

        //Creamos la petición
        xhr.open("PUT","http://localhost:8080/hackaton/products/" + this.updProduct.idp);

        console.log(JSON.stringify(this.updProduct));

        xhr.setRequestHeader("Content-Type", "application/json");

        //Enviamos la petición con el objeto nuevo
        xhr.send(JSON.stringify(this.updProduct));

        console.log("FIN newProduct");
    }

    getOrders(){
        console.log("Estoy en getOrders");
        console.log("Obteniendo datos de los pedidos");

        let xhr = new XMLHttpRequest();

        //Propiedad de XHR que se lanza cuando obtiene un resultado de la petición
        xhr.onload = () => {
            if (xhr.status === 200){
                console.log("Petición completada correctamente");
                
                //Parseamos el JSON que nos llega para visualizarlo en la consola
                console.log(JSON.parse(xhr.responseText));

                //Metemos en APIResponde el JSON
                let APIResponse = JSON.parse(xhr.responseText);

                //Asignamos a movies el array que viene en results
                this.pedidos = APIResponse;

                //Esto es el hola mundo y lo movemos tal cual, es una cadena de texto.
                //this.holaMundo = xhr.responseText;
            }
        }

        //Creamos la petición
        xhr.open("GET","http://localhost:8080/hackaton/cestas");
        //Enviamos la petición
        xhr.send();


        console.log("FIN getOrders");

    }
}

customElements.define('tienda-data', TiendaData);