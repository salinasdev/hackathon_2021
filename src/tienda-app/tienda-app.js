import { html, LitElement } from "lit";
import '../tienda-header/tienda-header.js';
import '../tienda-main/tienda-main.js';
import '../tienda-footer/tienda-footer.js';


class TiendaApp extends LitElement {

    static get properties(){
        return {
            people: {type : Array}

        };

    }

    constructor(){
        super();

        this.people = [];
    }

    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
            <tienda-header></tienda-header>
            <div class="row">
                <tienda-main></tienda-main>
            </div>
            
            <tienda-footer></tienda-footer>
        `;
    }


}

customElements.define('tienda-app', TiendaApp);