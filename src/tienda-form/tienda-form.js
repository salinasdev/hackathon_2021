import { html, LitElement } from "lit";

class TiendaForm extends LitElement {

    static get properties(){
        return {
            producto: {type: Object},
            editingProduct: {type: Boolean}
        };

    }

    constructor(){
        super();

        this.resetFormData();
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div>
            <form id="miForm">
                <div class="form-group">
                    <label>Nombre del producto: </label>
                    <input @input="${this.updateName}" 
                           type="text" class="form-control" 
                           placeholder="Nombre del producto"
                           required 
                           .value="${this.producto.name}" 
                           ?disabled="${this.editingProduct}"/>
                </div>
                <div class="form-group">
                    <label>Descripción del producto: </label>
                    <textarea  @input="${this.updateDesc}" class="form-control" placeholder="Perfil" rows="5" .value="${this.producto.desc}"></textarea>
                </div>
                <div class="form-group">
                    <label>Cantidad</label>
                    <input @input="${this.updateCant}" type="number" class="form-control" placeholder="Cantidad" .value="${this.producto.cant}" required/>
                    <label>Precio</label>
                    <input @input="${this.updatePrice}" type="number" class="form-control" placeholder="Precio" .value="${this.producto.price}" required/>
                </div>
                <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                <button @click="${this.storeProduct}" class="btn btn-success"><strong>Guardar</strong></button>
            </form>
        </div>
        `;
    }
    updated(changedProperties){
        console.log("updated de tienda-form");

        if (changedProperties.has("producto")){
            console.log("Ha cambiado el valor de la propiedad producto en tienda-form");

            console.log(this.producto);
        }

        if (changedProperties.has("editingProduct")){
            console.log("Ha cambiado el valor de la propiedad editingProduct en tienda-form");

            console.log(this.editingProduct);
        }
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);

        this.producto.name = e.target.value;
    }

    updateDesc(e){
        console.log("updateDesc");
        console.log("Actualizando la propiedad desc con el valor " + e.target.value);

        this.producto.desc = e.target.value;
    }

    updateCant(e){
        console.log("updateCant");
        console.log("Actualizando la propiedad cant con el valor " + e.target.value);

        this.producto.cant = e.target.value;
    }

    updatePrice(e){
        console.log("updatePrice");
        console.log("Actualizando la propiedad price con el valor " + e.target.value);

        this.producto.price = e.target.value;
    }

    resetFormData(){
        console.log("resetFormData");
        this.producto = {};
        this.producto.name = "";
        this.producto.desc = "";
        this.producto.cant = "";
        this.producto.price = "";
        this.editingProduct = false;
        console.log(this.producto);

        //this.document.getElementById("miForm").reset();
        
    }

    goBack(e){
        console.log("goBack de tienda-form");

        //Quitamos el submit del formulario:
        e.preventDefault();
 
        this.dispatchEvent(new CustomEvent("product-form-close",{}));
    }

    storeProduct(e){
        console.log("storeProduct de tienda-form");

        //Quitamos el submit del formulario:
        e.preventDefault();

        console.log("La propiedad name vale  " + this.producto.name);
        console.log("La propiedad desc vale  " + this.producto.desc);
        console.log("La propiedad cant vale  " + this.producto.cant);
        console.log("La propiedad price vale " + this.producto.price);

        this.dispatchEvent(
            new CustomEvent(
                "product-form-store", {
                    detail: {
                        product: {
                            idp: this.producto.idp,
                            name: this.producto.name,
                            desc: this.producto.desc,
                            cant: this.producto.cant,
                            price: this.producto.price
                        },
                        editingProduct : this.editingProduct
                    }
                }
                
            )
        );

    }
}

customElements.define('tienda-form', TiendaForm);