import { html, LitElement } from "lit";

class TiendaPedidos extends LitElement {

    static get properties(){
        return {
            idp: {type: Number},
            count: {type: String},
            amount: {type: String},
            colla: {type: Boolean},
            mipedido: {type: Array}

        };

    }

    constructor(){
        super();
        this.colla = false;
        this.mipedido = {};
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

        <div class="row mb-3">
            <div class="col-3 themed-grid-col bg-light">${this.idp}</div>
            <div class="col-4 themed-grid-col bg-light">${this.count} productos.</div>
            <div class="col-4 themed-grid-col bg-light">${this.amount} euros.</div>
            <div class="col-1 themed-grid-col bg-light"><button @click="${this.mostrarMas}" type="button" class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">+</button></div>
            <div class="collapse" id="collapseExample">
              <div class="card card-body">
              ${this.mipedido.map(
                pedido => html`- ${pedido.name}, cantidad ${pedido.cant} a ${pedido.price} euros la unidad.<br>`
              )}
              </div>
          </div>
        </div>



        <style>
        .themed-grid-col {
          padding-top: .75rem;
          padding-bottom: .75rem;
          background-color: rgba(86, 61, 124, .15);
          border: 1px solid rgba(86, 61, 124, .2);
        }
        </style>
        `;
    }

    mostrarMas(e){
      console.log("mostrarMas");
      if(!this.colla){
        this.shadowRoot.getElementById("collapseExample").classList.remove("collapse");
        this.colla = true;
      }else{
        this.shadowRoot.getElementById("collapseExample").classList.add("collapse");
        this.colla = false;
      }
        
    }
}

customElements.define('tienda-pedidos', TiendaPedidos);