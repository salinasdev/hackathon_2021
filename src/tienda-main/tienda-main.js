import { html, LitElement } from "lit";
import '../tienda-data/tienda-data.js';
import '../tienda-ficha-producto/tienda-ficha-producto.js';
import '../tienda-sidebar/tienda-sidebar.js';
import '../tienda-form/tienda-form.js';
import '../tienda-pedidos/tienda-pedidos.js';

class TiendaMain extends LitElement {

    static get properties(){
        return {
            products: {type: Array},
            showProductForm: {type: String},
            info: {type: Boolean},
            pedidos: {type: Boolean}
        };

    }

    constructor(){
        super();

        this.showProductForm = "productList";
        this.info = false;

        this.products = [];
        this.pedidos = [];
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <tienda-sidebar @new-product="${this.newProduct}"
                        @show-orders="${this.showOrders}"
        ></tienda-sidebar>
        
        <div class="row" id="productList">
        <h2 class="text-center">Productos</h2>
            <div class="row row-cols-1 row-cols-sm-4">
            ${this.products.map(
                product => html`<tienda-ficha-producto
                                idp="${product.id}"
                                name="${product.name}" 
                                cant="${product.cant}"
                                desc="${product.desc}"
                                price="${product.price}"
                                .photo="${{
                                    src: "../img/"+ product.name +".png",
                                    alt: "${product.name}"
                                }}"
                                @delete-product="${this.deleteProduct}"
                                @update-product="${this.updateProduct}"
                            ></tienda-ficha-producto>`
            )}
            </div>
        </div>
        <div class="row">
                <tienda-form @product-form-store="${this.productFormStore}" 
                              @product-form-close="${this.goBack}" 
                              class="d-none border rounded border-primary" 
                              id="productForm">
                </tienda-form>
        </div>
        
        <div id="orderList" class="d-none">
        <div class="row mb-3">
            <div class="col-3 themed-grid-col-cab bg-light">Número de pedido</div>
            <div class="col-4 themed-grid-col-cab bg-light">Items</div>
            <div class="col-4 themed-grid-col-cab bg-light">Precio Total</div>
            <div class="col-1 themed-grid-col-cab bg-light"></div>
        </div>
        ${this.pedidos.map(
            pedido => html`<tienda-pedidos
                            idp="${pedido.id}"
                            count="${pedido.productos.length}" 
                            amount="${this.dameTotal(pedido.productos)}"
                            .mipedido="${pedido.productos}"
                            }}"
                        ></tienda-pedidos>`
        )}
        <button @click="${this.volverAtras}" class="btn btn-primary"><strong>Volver</strong></button>
        </div>
        
        <tienda-data @products-data-updated="${this.getProducto}"
                     @orders-data-updated="${this.getOrders}"
                    id="tienda-data"
        ></tienda-data>
        <style>
        .themed-grid-col-cab {
          padding-top: .75rem;
          padding-bottom: .75rem;
          background-color: rgba(86, 61, 124, .15);
          border: 1px solid rgba(86, 61, 124, .2);
        }
        </style>
        `;
    }

    updated(changedProperties){
        console.log("updated de tienda-main");

        if (changedProperties.has("showProductForm")){
            console.log("Ha cambiado el valor de la propiedad showProductForm en persona-main");
            console.log("showProductForm vake " + this.showProductForm);

            switch (this.showProductForm){
                case "productList":
                        console.log("entro en productList");
                        this.showProductformList();
                    break;
                case "productForm":
                        console.log("entro en productForm");
                        if (!this.info){
                            this.shadowRoot.getElementById("productForm").producto = {name:'',desc:'',cant:'',price:''};
                        }
                        this.showProductformData();
                    break;
                case "orderList":
                        console.log("entro en orderList");
                        this.showOrderformList();
                    break;
            }


        }
    }

    

    getProducto(e){
        console.log("getProducto en tienda-main");
        console.log(e.detail.products);

        this.products = e.detail.products;

    }

    deleteProduct(e){
        console.log("deleteProduct en tienda-main del producto con id " + e.detail.idp);

        this.shadowRoot.getElementById("tienda-data").delProd = e.detail.idp;
    }

    newProduct(e){
        console.log("newProduct en tienda-main");
        this.shadowRoot.querySelector("tienda-form").editingProduct = false;
        this.showProductForm = "productForm";
        
    }

    showProductformData(){
        console.log("showProductformData");
        console.log("Mostrando Formulario del producto");

        this.shadowRoot.getElementById("productForm").classList.remove("d-none");
        this.shadowRoot.getElementById("productList").classList.add("d-none");
        this.shadowRoot.getElementById("orderList").classList.add("d-none");

    }

    showProductformList(){
        console.log("showProductformList");
        console.log("Mostrando listado de productos");
        this.shadowRoot.getElementById("productForm").classList.add("d-none");
        this.shadowRoot.getElementById("productList").classList.remove("d-none");
        this.shadowRoot.getElementById("orderList").classList.add("d-none");
    }

    showOrderformList(){
        console.log("showProductformList");
        console.log("Mostrando listado de productos");
        this.shadowRoot.getElementById("productForm").classList.add("d-none");
        this.shadowRoot.getElementById("productList").classList.add("d-none");
        this.shadowRoot.getElementById("orderList").classList.remove("d-none");

    }

    goBack(e){
        console.log("goBack desde tienda-main");
        this.showProductForm = "productList";

    }

    productFormStore(e){
        console.log("productFormStore en tienda-main");
        console.log(e.detail.product);
        console.log("editingProduct: " + e.detail.editingProduct);
        this.showProductForm = "productList";

        if (e.detail.editingProduct){
            console.log("Entro por UPDATE");
            this.shadowRoot.getElementById("tienda-data").updProduct = e.detail.product;
        }else{
            console.log("Entro por INSERT");
            this.shadowRoot.getElementById("tienda-data").product = e.detail.product;
        }
        

    }

    updateProduct(e){
        console.log("updateProduct en tienda-main del producto " + e.detail.idp);
        console.log("info ..................: " + e.detail.info);
        this.info = e.detail.info;
        this.shadowRoot.querySelector("tienda-form").producto = {idp: e.detail.idp ,name:e.detail.name,desc:e.detail.desc,cant:e.detail.cant,price:e.detail.price};
        this.shadowRoot.querySelector("tienda-form").editingProduct = true;
        console.log(this.shadowRoot.querySelector("tienda-form").producto);
        this.showProductForm = "productForm";

    }

    showOrders(e){
        console.log("showOrders en tienda-main");
        this.shadowRoot.querySelector("tienda-data").damePedido = true;
        this.showProductForm = "orderList";

    }

    getOrders(e){
        console.log("getOrders en tienda-main");
        this.pedidos = e.detail.pedidos;
        //this.shadowRoot.querySelector("tienda-pedidos").damePedido = true;
        //console.log(this.pedidos[0].productos);

    }

    dameTotal(array){
        let precio = 0
        for (let i = 0; i < array.length; i++) {
            console.log("Precio: " + array[i].price);
            precio+=array[i].price;
        }
        return precio;
    }

    volverAtras(){
        console.log("volverAtras");
        this.showProductForm = "productList";

    }
}

customElements.define('tienda-main', TiendaMain);