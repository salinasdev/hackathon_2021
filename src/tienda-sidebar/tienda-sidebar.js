import { html, LitElement } from "lit";

class TiendaSidebar extends LitElement {

    static get properties(){
        return {

        };

    }

    constructor(){
        super();
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
        <aside>
            <section>
                <div class="card-footer">
                    <button @click="${this.newProduct}" class="btn btn-secondary col-3 "><strong>Añadir Producto</strong></button>
                    <button @click="${this.showOrders}" class="btn btn-secondary col-3 offset-1"><strong>Ver Pedidos</strong></button>
                    <button class="btn btn-secondary col-3 offset-1"><strong>Ver Cesta</strong></button>
                </div>
            </section>
        </aside>
        
        `;
    }

    newProduct(e){
        console.log("newProduct en tienda-sidebar");

        //Evento que no manda info, simplemente el hecho de que se ha pulsado el boton
        this.dispatchEvent(new CustomEvent("new-product",{}));
    }

    showOrders(e){
        console.log("showOrders en tienda-sidebar");

        //Evento que no manda info, simplemente el hecho de que se ha pulsado el boton
        this.dispatchEvent(new CustomEvent("show-orders",{}));
    }
}

customElements.define('tienda-sidebar', TiendaSidebar);