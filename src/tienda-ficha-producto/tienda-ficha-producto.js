import { html, LitElement } from "lit";

class TiendaFichaProducto extends LitElement {

    static get properties(){
        return {
            idp: {type: Number},
            name: {type: String},
            desc: {type: String},
            price: {type: Number},
            cant: {type: Number},
            photo: {type: Object}
        };

    }

    constructor(){
        super();
    }

    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div class="card h-100">
            <img src="${this.photo.src}" alt="${this.photo.alt}"  class="card-img-top" onerror="this.onerror=null;this.src='../img/Otros.png';">
            <div class="card-body">
                <h5 class="card-title">${this.name}</h5>
                <p class="card-text">${this.desc}</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Stock ${this.cant}.</li>
                    <li class="list-group-item">Precio ${this.price} euros.</li>
                </ul>
            </div>
            <div class="card-footer">
                <button @click="${this.deleteProduct}" class="btn btn-outline-danger col-5"><strong>X</strong></button>
                <button @click="${this.moreInfo}" class="btn btn-outline-info col-5 offset-1"><strong>Info</strong></button>
            </div>
        </div>
        `;
    }

    deleteProduct(e){
        console.log("deleteProduct en tienda-ficha-producto");
        console.log("Se va a borrar el producto con id " + this.idp);

        this.dispatchEvent(
            new CustomEvent(
                "delete-product",
                {
                    "detail" : {
                        idp : this.idp
                    }
                }
            )
        );
    }

    moreInfo(e){
        console.log("moreInfo");
        console.log("Se va a mostrar la info del producto con id " + this.idp);

        this.dispatchEvent(
            new CustomEvent(
                "update-product",
                {
                    "detail" : {
                        idp : this.idp,
                        name: this.name,
                        desc: this.desc,
                        cant: this.cant,
                        price: this.price,
                        info: true
                    },
                    
                }
            )
        );
    }
}

customElements.define('tienda-ficha-producto', TiendaFichaProducto);